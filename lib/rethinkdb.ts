import { r, R, RDatabase, RPoolConnectionOptions } from 'rethinkdb-ts';


let client: R = null;

const getClient = async (options?: RPoolConnectionOptions): Promise<R> => {
    // If the client is null
    if (client === null) {
        // Assign "r" to client value
        client = r
        // And connect to rethinkdb servers using a connection pool
        await client.connectPool(options)
    }
    return client
}


const db = async (name: string): Promise<RDatabase> => {
    if (client) {
        return await (await getClient()).db(name)
    }
    throw new Error("Client has not been initialized yet")
}

export {
    getClient,
    db
}
