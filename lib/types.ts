export interface Command {
    date: Date,
    name: string,
    address: string,
}
