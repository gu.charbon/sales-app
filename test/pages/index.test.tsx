/**
 * Test scenarios for the home page.
 */
import React from 'react'
import { render, fireEvent } from '../testUtils'
import Home from '../../pages/index'

/**
 * Home page testing
 */
describe('Home page', () => {
  /**
   * Test that button triggers an alert when clicked
   */
  it('clicking button triggers alert', () => {
    const { getByText } = render(<Home />, {})
    // Mock the windows.alert function
    window.alert = jest.fn()
    // Fire a click event
    fireEvent.click(getByText('Test Button'))
    // Expect some alert
    expect(window.alert).toHaveBeenCalledWith('Hello sales app')
  })
})
