/**
 * This module is used to export test functions.
 * It overrides the 'render' method offered by @testing-library/react and export eveything else untouched.
 * 
 * It is not useful for the moment, but if we want to wrap the children to render using i18n provider or a theme provider
 * most of the work will already be done.
 */
import { render } from '@testing-library/react'
import { ThemeProvider } from 'theme-ui'
import { tailwind } from "@theme-ui/presets"

// Not used for now, might be useful later to wrap page using a ThemeProvider for example.
const Providers = ({ children }) => {
  return (
    <ThemeProvider theme={tailwind}>   
    {children}
    </ThemeProvider>

  )
}

// We don't care about types in the next line
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const _customRender = (ui, options = {}) =>
  // We must use a custom render in order to wrap using providers
  render(ui, { wrapper: Providers, ...options })

// Because we force customRender to have same type than render function from @testing-library/react
const customRender = _customRender as typeof render

// re-export everything from testing-library
export * from '@testing-library/react'

// override render method
export { customRender as render }
