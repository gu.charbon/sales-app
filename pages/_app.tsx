import React from 'react'
import { AppProps } from 'next/app'
import { ThemeProvider } from 'theme-ui'
import { tailwind } from "@theme-ui/presets"

const App = ({ Component, pageProps }: AppProps): JSX.Element => {
  return (
    <ThemeProvider theme={tailwind}>
      <Component {...pageProps} />
    </ThemeProvider>
  )
}

export default App
