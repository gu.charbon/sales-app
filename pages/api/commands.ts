// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { NextApiRequest, NextApiResponse } from 'next'
import { getClient } from '../../lib/rethinkdb';
import { Command } from '../../lib/types'
import { Logger } from 'tslog'

// Create a new logger instance to display log messages in the console
const logger = new Logger({name: "api", minLevel: "debug"})


/**
 * Handler for /api/commands endpoint.
 * 
 * Documentation:
 *   - [An overview of HTTP](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview)
 *   - [NextJS API Routes](https://nextjs.org/docs/api-routes/introduction)
 * 
 * @param req : The HTTP request received as input
 * @param res : The HTTP response to send back
 */
const handler = async (req: NextApiRequest, res: NextApiResponse): Promise<void> => {
    // Get connection to the database
    const client = await getClient()
    // Get database named "demo"
    const db = client.db("demo")
    // Get table named "commands" inside the database named "demo"
    const table = db.table("commands")

    // If the user performed a GET request
    if (req.method == "GET") {
        // By default set HTTP_500_INTERNAL_ERROR
        res.statusCode = 500
        // Query all commands from database
        const commands = await table.run()
        // Set HTTP_200_OK if request succeed
        res.statusCode = 200
        // Set response body with list of commands
        res.json({ commands: commands })

    // If the user performed a POST request
    } else if (req.method == "POST") {
        // By default set HTTP_500_INTERNAL_ERROR
        res.statusCode = 500
        // Extract new command data from request body
        const newCommand = req.body as Command;
        // Display a message in the console to debug
        logger.debug("Inserting new command:", newCommand)
        // Insert command into database
        await table.insert(newCommand).run()
        // Set response body
        res.json({"status": "inserted"})
        // Set HTTP_202_OK if request succeed
        res.statusCode = 202
    }

    // End the request
    res.end()
}

export default handler
