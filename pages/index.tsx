import Head from 'next/head'
import { Box, Heading, Button, Flex } from 'rebass'

// A page is nothing more than a function
const Home = (): JSX.Element => {
  // which returns some JSX element (React HTML syntax)
  return (
    <div>
      {/* The head of the page can be configured using the <Head> component */}
      <Head>
        <title>Sales App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {/* Use elements offered by rebass UI library */}
      <Box>
        <Heading>Hello</Heading>
        <Flex
          width="full"
          flexDirection="column"
          py="1rem"
          px="1rem"
          justifyItems="center"
          justifyContent="center"
          alignContent="center"
          alignItems="center"
        >
          <Button
            my="2rem"
            width="50%"
            onClick={async () => {
              const response = await fetch('/api/commands', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({ name: 'demo' }),
              })
              const commands = await response.json()
              window.alert(JSON.stringify(commands))
            }}
          >
            Add
          </Button>
          <Button
            my="2rem"
            width="50%"
            onClick={async () => {
              const response = await fetch('/api/commands')
              const commands = await response.json()
              window.alert(JSON.stringify(commands))
            }}
          >
            List
          </Button>
        </Flex>
      </Box>
    </div>
  )
}

export default Home
